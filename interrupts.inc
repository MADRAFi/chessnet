(* declare your interrupt routines here *)

procedure dli1;assembler;interrupt;
asm {
    pha ; store registers

    mva #BGCOLOR_BOARD colpf2
    mva #FGCOLOR_BOARD colpf1
    mva #.hi(CHARSET_ADDRESS) chbase
    mwa #dli2 VDSLST

    pla ; restore registers
};
end;

procedure dli2;assembler;interrupt;
asm {
    pha ; store registers

    lda #BGCOLOR_CONSOLE 
    sta wsync
    sta colpf2
    mva consoleCH chbase
    mva #FGCOLOR_CONSOLE colpf1
    mwa #dli1 VDSLST

    pla ; restore registers
};
end;



procedure vbl;assembler;interrupt;
asm {
    pha ; store registers
    mwa #dli1 VDSLST
    pla ; restore registers
    jmp $E462 ; jump to system VBL handler
};
end;
