program chessNet;
{$librarypath '../blibs/'}
// get your blibs here: https://gitlab.com/bocianu/blibs
uses atari, sysutils, crt, b_crt, fn_tcp;

const
{$i const.inc}
{$r resources.rc}
{$i interrupts.inc}
{$i tiles.inc}

var FNuri: PChar = 'N:TCP://freechess.org:5000/'#0; // #FujiNet uri
    sBuffer: string[INPUT_BUFFER_LENGTH];    // send buffer
    rLine: string[RECEIVE_LINE_BUFFER_LENGTH];      // last received line
    timeout: boolean;   
    muted: boolean;
    playerColor: byte;  // player pieces color 0 - black / 1 - white

    consoleDL: word;    // display list address of default console
    consoleVRAM: word;  // memory address of default console
    consoleCH: word;    // charset address of default console
    
    gameState: byte;
    screenState: byte;  // 0 - console / 1 - board

    board: array [0..7, 0..7] of byte;  // data from board parser goes here
    board_moveNum: string[4];
    board_whoMoves: string[6];
    board_lastMove: string[10];
    board_clockBlack: string[10];
    board_clockWhite: string[10];
    board_strengthBlack: string[4];
    board_strengthWhite: string[4];

    bline : TString;    // temporary vars
    b:byte;

// *************************************************************
// ************************************************************* BOARD / GUI ROUTINES
// *************************************************************

procedure SetScreenState(state: byte);
begin
    Pause;
    screenState := state;
    if state = SCREEN_BOARD then begin
        sdlstl := BOARD_DL_ADDRESS;
        chbas := Hi(CHARSET_ADDRESS);
    end else begin
        sdlstl := consoleDL;
        chbas := consoleCH;
    end;
end;

procedure ToggleBoard;
begin
    if screenState = SCREEN_BOARD then SetScreenState(SCREEN_CONSOLE)
    else SetScreenState(SCREEN_BOARD)
end;

procedure Echo(s:Tstring);
begin
    if not muted then WriteLn(s);
end;

procedure LineClear;
begin
    SetLength(rLine, 0);
end;

procedure LineAppend(c: byte);
begin
    SetLength(rLine, Length(rLine) + 1);
    rLine[Length(rLine)] := char(c);
end;

procedure LineShow;
begin
    Write(rLine);
    LineClear;
end;

function GetPieceEnum(c: char): byte;
begin
    result := NONE;
    case c of
        'P' : result := PIECE_P;
        'N' : result := PIECE_N;
        'B' : result := PIECE_B;
        'R' : result := PIECE_R;
        'Q' : result := PIECE_Q;
        'K' : result := PIECE_K;
    end;
end;

function ParsePiece(b1, b2: char): byte;
begin
    if b1 = ' ' then begin
        exit(0);   // empty
    end;
    if b1 = '*' then begin     // black piece
        result := $00 or GetPieceEnum(b2);

    end else begin             // white piece
        result := $10 or GetPieceEnum(b1);
    end;
end;

function console2Board(var s: Tstring): Tstring;
var i:byte;
begin
  result := Atascii2Antic(AnsiUpperCase(s));
  for i:=1 to Length(result) do Inc(result[i],#128);
end;

procedure WriteOnBoard(s:Tstring; offset: word);
begin
    s := console2Board(s);
    move(s[1],pointer(BOARD_RAM_ADDRESS+offset), byte(s[0]));
end;

procedure PutTile(x, y: byte; tile: pointer);
var a:array [0..0] of word;
    vram:word;
begin
    a := tile;
    vram := BOARD_RAM_ADDRESS + (((y shl  1) + 2) * 40) + 3 + (x shl 1);
    DPoke(vram, a[0]);
    DPoke(vram + 40, a[1]);
end;    

procedure DrawBoard;
var x, y, tile, bg, col, piece:byte;
    tiles: array [0..0] of pointer;
begin
    for y:=0 to 7 do begin
        for x:=0 to 7 do begin
            tile := board[x, y];
            if tile <> 0 then begin
                piece := (tile and 15) - 1;
                col := (tile and 16) shr 3;
                bg := (x xor y xor 1) and 1;
                tiles := pieces[col or bg];
                putTile(x, y, tiles[piece]);
            end;
        end;
    end;
    if playerColor <> NONE then begin
        if playerColor = COLOR_WHITE then bline := 'A B C D E F G H'
        else bline := 'H G F E D C B A';
        WriteOnBoard(bline,4);
        WriteOnBoard(bline,763);
        for y:=0 to 7 do begin
            if playerColor = COLOR_BLACK then x := 128 + 17 + y
            else x := 128 + 24 - y;
            Poke(BOARD_RAM_ADDRESS + ((y + 1) * 80) + 1, x);
            Poke(BOARD_RAM_ADDRESS + ((y + 1) * 80) + 60, x);
        end;
        WriteOnBoard('MOVE:',103);
        WriteOnBoard(board_moveNum,109);
        WriteOnBoard(board_whoMoves,113);

        WriteOnBoard('PLAYER BLACK',263);
        WriteOnBoard('TIME:',343);
        WriteOnBoard('STRENGTH:',383);
        WriteOnBoard(board_clockBlack,349);
        WriteOnBoard(board_strengthBlack,393);

        WriteOnBoard('PLAYER WHITE',503);
        WriteOnBoard('TIME:',583);
        WriteOnBoard('STRENGTH:',623);
        WriteOnBoard(board_clockWhite,589);
        WriteOnBoard(board_strengthWhite,633);
    end;
end;

procedure ClearBoard;
var vram : word;
    i: byte;
begin
    vram := BOARD_RAM_ADDRESS;
    fillByte(pointer(vram), 800, 128);
    // top border
    bline := #$d8#$7a#$7a#$7a#$7a#$7a#$7a#$7a#$7a#$7a#$7a#$7a#$7a#$7a#$7a#$7a#$7a#$d9;
    Move(bline[1], Pointer(vram + 42), 18);
    // checkerboard
    bline := #$00#$00#$80#$80#$00#$00#$80#$80#$00#$00#$80#$80#$00#$00#$80#$80#$00#$00;
    vram := BOARD_RAM_ADDRESS + 80;
    for i:=0 to 3 do begin
        Move(bline[3], pointer(vram + 3), 16);
        Move(bline[3], pointer(vram + 43), 16);
        Move(bline[1], pointer(vram + 83), 16);
        Move(bline[1], pointer(vram + 123), 16);
        vram := vram + 160;
    end;
    // border sides
    vram := BOARD_RAM_ADDRESS + 80;
    for i:=0 to 15 do begin
        Poke(vram + 2, $5A);
        Poke(vram + 19, $DA);
        Inc(vram, 40);
    end;
    // bottom border
    bline := #$f8#$fa#$fa#$fa#$fa#$fa#$fa#$fa#$fa#$fa#$fa#$fa#$fa#$fa#$fa#$fa#$fa#$f9;
    Move(bline[1], pointer(vram + 2), 18);
end;

procedure InitBoard;
begin
    lmargin:=0;
    color2 := BGCOLOR_CONSOLE;
    color1 := FGCOLOR_CONSOLE;
    consoleDL := sdlstl;
    consoleVRAM := savmsc;
    consoleCH := chbas;
    DPoke(BOARD_DL_ADDRESS+26, consoleVRAM + 800); // set board LMS for bottom console view
    Pause;
    SetIntVec(iDLI, @dli1);
    SetIntVec(iVBL, @vbl);    
    nmien := $c0;
    screenState := SCREEN_CONSOLE;
    gameState := GS_LOGIN;
end;

procedure ShowTitle;
begin
    ClrScr;
    Write(#155#155#155#155#155#155#155#155#155); // 9 breaklines
    Move(pointer(TITLE_ADDRESS), pointer(savmsc), 360);
end;

procedure Timeouted;
begin
    Writeln('*** Timeouted!'); 
end;

// ****************************************************************
// **************************************************************** DATA PARSING ROUTINES
// ****************************************************************

function ProcessBuffer: boolean;
var eol:boolean;
    b:byte;
begin
    eol := false;
    b := TCP_ReadByte;
    case b of 
        10: begin // LF - line feed
                LineAppend(155);
                eol := true;
            end;
        7:  Beep; // BELL 
        1:  begin  // SOH - start of Header
                LineAppend(156);
            end;
        13: ; // CR - ignore
        12: ; // FF - ignore
        125: ; // CLR - ignore
        else LineAppend(b) // output char
    end;
    if (Length(rLine) = RECEIVE_LINE_BUFFER_LENGTH) or (TCP_bufferLength = 0) then eol:=true;
    result := eol;
end;

function LineContains(s:Tstring): boolean;
var i, sIdx: byte;
begin
    result := false;
    sIdx := 1;
    for i:=1 to Length(rLine) do begin
        if sIdx > 1 then begin
            if s[sIdx] = rLine[i] then begin
              Inc(sIdx);
              if sIdx = Length(s) + 1 then exit(true);
            end else sIdx := 1;
        end else begin
          if s[sIdx] = rLine[i] then Inc(sIdx);
        end;
    end;
end;

procedure SubstrFromLine(pos:byte; var s:TString);
begin
    s:='';
    while ((rLine[pos] > #32) and (rLine[pos] < #123) and (pos <= Length(rline))) do begin
        Inc(s[0]);
        s[Length(s)] := rLine[pos];
        Inc(pos);
    end;
end;

function FindInLine(s:Tstring):byte;
var i, sIdx: byte;
begin
    sIdx := 1;
    for i:=1 to Length(rLine) do begin
        if sIdx > 1 then begin
            if s[sIdx] = rLine[i] then begin
                Inc(sIdx);
                if sIdx = Length(s) + 1 then exit(result);
            end else begin
                sIdx := 1;
                result := $ff;
            end;
        end else begin
            if s[sIdx] = rLine[i] then begin
                Inc(sIdx);
                result := i;
            end;
        end;
    end;
    result := $ff;
end;

function LineStartsWith(s: Tstring; var line: string): boolean;
var i: byte;
begin
    result := true;
    for i := 1 to Length(s) do begin
        if (i > Length(s)) or (i > Length(rLine)) then exit(false);
        if s[i] <> line[i] then exit(false);
    end;
end;

procedure WaitFor(s: TString);
var eol, found:boolean;
    time: word;
begin
    LineClear;
    timeout := false;
    found := false;
    time := 0;
    if not muted then Writeln('Waiting for ', s);
    repeat

        TCP_CheckAndPoll;
        if TCP_bufferLength > 0 then begin
            eol := ProcessBuffer;
            if eol then begin
                if LineContains(s) then found := true
                else LineClear;
            end;
        end else begin
            Pause; 
            Inc(time);
            timeout := time > TIMEOUT_FRAMES;
        end;

    until found or timeout;
    if timeout then Timeouted;
end;

function ParseBoard: boolean;
var boardRow,c,boardOffset:byte;
begin
    result := false;
    muted := true;
    WaitFor(' Move # : ');
    playerColor := NONE;
    boardRow := 0;
    if rLine[5] = '1' then playerColor := COLOR_BLACK;
    if rLine[5] = '8' then playerColor := COLOR_WHITE;
    muted := false;
    if playerColor = NONE then exit(false);
    Echo('Parsing Board...');
    muted := true;
    repeat 
        boardOffset := 10;
        for c:=0 to 7 do begin
            board[c,boardRow] := parsePiece(rLine[boardOffset],rLine[boardOffset+1]);
            Inc(boardOffset,4)
        end;
        case boardRow of
            0: begin
                SubstrFromLine(55, board_moveNum);
                c := FindInLine(' (');
                SubstrFromLine(c + 2, board_whoMoves);
                Dec(board_whoMoves[0]);
            end;
            1: SubstrFromLine(61, board_lastMove);
            3: SubstrFromLine(60, board_clockBlack);
            4: SubstrFromLine(60, board_clockWhite);
            5: SubstrFromLine(63, board_strengthBlack);
            6: SubstrFromLine(63, board_strengthWhite);
        end;
        Inc(boardRow);
        LineClear;
        if boardRow < 8 then WaitFor('  | ');
    until boardRow = 8;
    WaitFor('fics%');
    ClearBoard;
    DrawBoard;
    SetScreenState(SCREEN_BOARD);
    result := true;
    muted := false;
end;

procedure ProcessLine;
var slen,rlen:byte;
begin
    if LineStartsWith('       -----', rLine) then ParseBoard;
    if LineStartsWith('Logging you out.', rLine) then gameState := GS_QUIT;
    if LineStartsWith('fics% ', rLine) then begin // rewrite user input at prompt
        slen := Length(sBuffer);
        if slen > 0 then begin
            rlen := Length(rLine);
            move(sBuffer[1], rline[rlen+1], slen);
            rLine[0] := char(rlen + slen);
        end;
    end;
    if not muted then Write(rLine);
    LineClear;
end;

// *****************************************************
// *****************************************************  KEYBOARD ROUTINES
// *****************************************************

procedure InputAppend(c: char);
begin 
    SetLength(sBuffer, Length(sBuffer) + 1);
    sBuffer[Length(sBuffer)] := c;
end;

function ProcessKeypress(c: char): boolean;
var len:byte;
Begin
    Result := false;
    len := Length(sBuffer);
    if len >= INPUT_BUFFER_LENGTH then exit;
    If ord(c) = 155 Then // RETURN
        Begin
            InputAppend(char(10));
            TCP_SendString(sBuffer);
            Writeln;
            SetLength(sBuffer,0);
            exit(true);
        End;
    if ord(c) = 127 then // TAB
        Begin
            ToggleBoard;
            exit(false);
        End;
    if ord(c) = 126 then // BKSPC
        Begin
            if len > 0 then begin
                SetLength(sBuffer,len - 1);
                Write(c);
            end;
            exit(false);
        End;
    // 27 - esc
    InputAppend(c);
    Write(c);
End;

procedure WaitForUserInput;
var eol:Boolean;
begin
    repeat
        TCP_CheckAndPoll;
        if TCP_bufferLength > 0 then 
            if ProcessBuffer then ProcessLine;
        If keypressed Then eol := ProcessKeypress(ReadKey);
    until eol or (TCP_status.connected = TCP_DISCONNECTED);
end;

// **********************************************************************
// *******************************************************************************  MAIN
// **********************************************************************

begin
    InitBoard;
    ClearBoard;
    ShowTitle;

    Writeln('Connecting: ', FNuri);
    ioResult := TCP_Connect(FNuri);

    if ioResult <> 1 then begin // error
        Writeln('Connection Error: ', ioResult);
        Readkey;
    end else begin  // Connected  - start the game
    
        TCP_AttachIRQ;
        muted := true; // hide all server console output

        WaitFor('version');     // but display server version
        b := FindInLine(' S');
        if b < $ff then rLine[b] := #155; // breakline
        LineShow;

        WaitFor('login');
        LineShow;
        muted := false;  // display all following messages
        WaitForUserInput;

        // main network/game loop 
        repeat                      
            TCP_CheckAndPoll;
            if TCP_status.connected = TCP_DISCONNECTED then gameState := GS_QUIT;
            if TCP_bufferLength > 0 then 
                if ProcessBuffer then ProcessLine;
            if keypressed Then ProcessKeypress(ReadKey);
        until gameState = GS_QUIT;

        Writeln('Closing connection...');
    end;

    TCP_DetachIRQ;
    TCP_Close;
    TextMode(0);
end.
